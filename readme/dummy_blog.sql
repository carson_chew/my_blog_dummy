-- Adminer 4.7.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `topics`;
CREATE TABLE `topics` (
  `tp_id` int(5) NOT NULL AUTO_INCREMENT,
  `tp_up_id` int(5) NOT NULL COMMENT 'PK from user_profile',
  `tp_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tp_body` text NOT NULL,
  `tp_created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `topics` (`tp_id`, `tp_up_id`, `tp_title`, `tp_body`, `tp_created_date`) VALUES
(2,	1,	'Regenerate session ID, optionally destroying the current session data.',	'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quo studio Aristophanem putamus aetatem in litteris duxisse? <b><u>Quae cum essent dicta, discessimus</u></b>. Qua ex cognitione facilior facta est investigatio rerum occultissimarum. Aliter enim nosmet ipsos nosse non possumus. D<sub>uo Reges: constructio interrete. Id est enim, de quo quaerimus. </sub><br></p>',	'2019-12-06 07:08:35'),
(3,	2,	'外星人？还是秘术的产物？',	'<p>Beatus sibi videtur esse moriens. Sin laboramus, quis est, qui alienae modum statuat industriae? Si enim ad populum me vocas, eum. Sed quid sentiat, non videtis. Eam stabilem appellas. Cur post Tarentum ad Archytam? <br></p>',	'2019-12-06 08:38:01'),
(4,	1,	'Also be used with sql max function',	'<p><span class=\"st\">Definition and Usage. The <em>MAX</em>() function returns the <em>maximum</em> value in a set of values. Note: See also the MIN() function</span></p>',	'2019-12-06 09:36:51'),
(5,	1,	'Quis enim est, qui non videat haec esse in natura rerum tria?',	'<p>Qui convenit? Quid, si etiam iucunda memoria est praeteritorum malorum? Tibi hoc incredibile, quod beatissimum. <br></p>',	'2019-12-06 09:59:17'),
(6,	1,	'Neque solum ea communia, verum etiam paria esse dixerunt.',	'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid enim? Quod cum dixissent, ille contra. Sed ad bona praeterita redeamu<i>s. Hoc tu nunc in illo probas. </i><br><br>Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae.<br></p>',	'2019-12-06 09:59:36');

DROP TABLE IF EXISTS `topic_comment`;
CREATE TABLE `topic_comment` (
  `tc_id` int(5) NOT NULL AUTO_INCREMENT,
  `tc_tp_id` int(5) NOT NULL COMMENT 'PK from topics',
  `tc_up_id` int(5) NOT NULL COMMENT 'PK from user_profile',
  `tc_content` text NOT NULL,
  `tc_created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `topic_comment` (`tc_id`, `tc_tp_id`, `tc_up_id`, `tc_content`, `tc_created_date`) VALUES
(1,	3,	2,	'<p>Nemo nostrum istius generis asotos iucunde putat vivere. Maximus dolor, inquit, brevis est. Confecta res esset. Graece<strike> donan, Latine voluptatem vocant. Tu quidem reddes; </strike><br></p>',	'2019-12-06 09:05:45'),
(3,	3,	2,	'<p>Res enim concurrent contrariae. Istam voluptatem p<sub>erpetuam quis potest praestare sapienti</sub>? Erat enim Polemonis. Sed fortuna fortis; <br></p>',	'2019-12-06 09:08:59'),
(6,	2,	2,	'<p>Tollitur beneficium, tollitur gratia, quae sunt vincla concordiae. Conferam avum tuum Drusum cum C. Quantum Aristoxeni ingenium consumptum videmus in musicis? Philosophi autem in suis lectulis plerumque moriuntur. Beatum, inquit. <br></p>',	'2019-12-06 09:24:41'),
(7,	3,	1,	'<p>Qui est in parvis malis. Eadem <span style=\"background-color: rgb(255, 255, 0);\">fortitudinis ratio reperietur. Quippe</span>: habes enim a rhetoribus; Polycratem Samium felicem appellabant. An eiusdem modi? <br></p>',	'2019-12-06 09:28:51'),
(8,	4,	1,	'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Si longus, levis dictata sunt. Ut pulsi recurrant? At iam decimum annum in spelunca iacet. Eam stabilem appellas. Duo Reges: constructio interrete. Egone quaeris, inquit, quid sentiam? <br></p>',	'2019-12-06 09:37:01'),
(9,	4,	1,	'<p>Zenonis est, inquam, hoc Stoici. Erat enim res aperta. <br></p>',	'2019-12-06 09:37:07'),
(10,	4,	1,	'<p>Quid enim possumus hoc agere divinius? Compensabatur, inquit, cum summis doloribus laetitia. Contemnit enim disserendi elegantiam, confuse loquitur. Ut pulsi recurrant? <br></p>',	'2019-12-06 09:37:13'),
(11,	4,	1,	'<p>Cur deinde Metrodori liberos commendas? Faceres tu quidem, Torquate, haec omnia; Negat enim summo bono afferre incrementum diem. Cum audissem Antiochum, Brute, ut solebam, cum M. <br></p>',	'2019-12-06 09:37:20'),
(12,	4,	1,	'<p>Explanetur igitur. Avaritiamne minuis? Tollenda est atque extrahenda radicitus. Odium autem et invidiam facile vitabis. Huius ego nunc auctoritatem sequens idem faciam.<br></p>',	'2019-12-06 09:37:28'),
(13,	6,	1,	'<p>Peccata paria. Deinde disputat, quod cuiusque generis animantium statui deceat extremum. Comprehensum, quod cognitum non habet? <br><br>Dat enim intervalla et relaxat. Quodsi ipsam honestatem undique pertectam atque absolutam. Si quicquam extra virtutem habeatur in bonis. Verum hoc idem saepe faciamus. <br></p>',	'2019-12-06 09:59:54'),
(14,	6,	1,	'<p>Minime vero, inquit ille, consentit. Erat enim res aperta. Sit enim idem caecus, debilis. Suo genere perveniant ad extremum; <br><br>Ut aliquid scire se gaudeant? Num quid tale Democritus? Quae est igitur causa istarum angustiarum? Tum ille: Ain tandem? Erat enim Polemonis. <br><br>Sed fortuna fortis; Quo modo autem philosophus loquitur? Tum mihi Piso: Quid ergo? Sequitur disserendi ratio cognitioque naturae; <br><br>Duo Reges: constructio interrete. Nihil illinc huc pervenit. <br></p>',	'2019-12-06 10:00:00'),
(15,	5,	1,	'<p>Videsne quam sit magna dissensio? Hoc mihi cum tuo fratre convenit. At ille pellit, qui permulcet sensum voluptate. Nunc de hominis summo bono quaeritur; Negat esse eam, inquit, propter se expetendam. <br><br>Neque solum ea communia, verum etiam paria esse dixerunt. Nunc agendum est subtilius. Summum a vobis bonum voluptas dicitur. Utilitatis causa amicitia est quaesita. Deprehensus omnem poenam contemnet. Tum Torquatus: Prorsus, inquit, assentior; Quis hoc dicit? Simus igitur contenti his. <br></p>',	'2019-12-06 10:00:15'),
(16,	5,	1,	'<p>Quod quidem iam fit etiam in Academia. Omnia contraria, quos etiam insanos esse vultis. Quod quidem nobis non saepe contingit. Bonum incolumis acies: misera caecitas. Itaque contra est, ac dicitis; Scrupulum, inquam, abeunti; <br></p>',	'2019-12-06 10:00:22');

DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE `user_profile` (
  `up_id` int(5) NOT NULL AUTO_INCREMENT,
  `up_name` varchar(50) NOT NULL,
  `up_password` varchar(255) NOT NULL,
  PRIMARY KEY (`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user_profile` (`up_id`, `up_name`, `up_password`) VALUES
(1,	'carson',	'$2y$10$6mXD0eMSVIA4f5G0CM/BOuXWyfEZ75g5Q1VzZpXvAA/WRNIC3JpMa'),
(2,	'limpek',	'$2y$10$tO6/hT0o0rC9vxKyUu.vQOEIF84bIV9vcYfskQx7s71v0uu6hioBK');

-- 2019-12-06 10:12:10
